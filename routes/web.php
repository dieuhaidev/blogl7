<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('admin/login','Admin\AdminController@adminLogin');
Route::post('admin/login','Admin\AdminController@postAdminLogin')->name('admin_login');


Auth::routes();



Route::group(['prefix' => 'admin'],function () {
    Route::get('/',                     'Admin\AdminController@index');

    Route::group(['prefix' => 'categories'], function() {
        Route::get('/',[
            'as'=> 'categories.index',
            'uses'=> 'Admin\CategoryController@index'
        ]);

        Route::get('/create',[
            'as'=> 'categories.create',
            'uses'=> 'Admin\CategoryController@create'
        ]);
        Route::post('/store',[
            'as'=> 'categories.store',
            'uses'=> 'Admin\CategoryController@store'
        ]);

        Route::get('/edit/{id}',[
            'as'=> 'categories_edit',
            'uses'=> 'Admin\CategoryController@edit'
        ]);
        Route::post('/update/{id}',[
            'as'=> 'categories_update',
            'uses'=> 'Admin\CategoryController@update'
        ]);

        Route::get('/delete/{id}',[
            'as'=> 'categories_delete',
            'uses'=> 'Admin\CategoryController@delete'
        ]);
    });

    Route::group(['prefix' => 'menus'], function() {
        Route::get('/',[
            'as'=> 'menus.index',
            'uses'=> 'Admin\MenuController@index'
        ]);

        Route::get('/create',[
            'as'=> 'menus.create',
            'uses'=> 'Admin\MenuController@create'
        ]);
        Route::post('/store',[
            'as'=> 'menus.store',
            'uses'=> 'Admin\MenuController@store'
        ]);

        Route::get('/edit/{id}',[
            'as'=> 'menu_edit',
            'uses'=> 'Admin\MenuController@edit'
        ]);
        Route::post('/update/{id}',[
            'as'=> 'menu_update',
            'uses'=> 'Admin\MenuController@update'
        ]);

        Route::get('/delete/{id}',[
            'as'=> 'menu_delete',
            'uses'=> 'Admin\MenuController@delete'
        ]);
    });

    Route::group(['prefix' => 'products'], function() {
        Route::get('/',[
            'as'=> 'products.index',
            'uses'=> 'Admin\ProductsController@index'
        ]);
        Route::get('/view/{id}',[
            'as'=> 'product_view',
            'uses'=> 'Admin\ProductsController@view'
        ]);
        Route::get('/create',[
            'as'=> 'products.create',
            'uses'=> 'Admin\ProductsController@create'
        ]);
        Route::post('/store',[
            'as'=> 'products.store',
            'uses'=> 'Admin\ProductsController@store'
        ]);

        Route::get('/edit/{id}',[
            'as'=> 'product_edit',
            'uses'=> 'Admin\ProductsController@edit'
        ]);
        Route::post('/update/{id}',[
            'as'=> 'product_update',
            'uses'=> 'Admin\ProductsController@update'
        ]);

        Route::get('/delete/{id}',[
            'as'=> 'product_delete',
            'uses'=> 'Admin\ProductsController@delete'
        ]);
    });

    Route::group(['prefix' => 'slider'], function() {
        Route::get('/',[
            'as'=> 'slider.index',
            'uses'=> 'Admin\SliderController@index'
        ]);

        Route::get('/create',[
            'as'=> 'slider.create',
            'uses'=> 'Admin\SliderController@create'
        ]);
        Route::post('/store',[
            'as'=> 'slider.store',
            'uses'=> 'Admin\SliderController@store'
        ]);

        Route::get('/edit/{id}',[
            'as'=> 'slider_edit',
            'uses'=> 'Admin\SliderController@edit'
        ]);
        Route::post('/update/{id}',[
            'as'=> 'slider_update',
            'uses'=> 'Admin\SliderController@update'
        ]);

        Route::get('/delete/{id}',[
            'as'=> 'slider_detete',
            'uses'=> 'Admin\SliderController@delete'
        ]);
    });

    Route::group(['prefix' => 'settings'], function() {
        Route::get('/',[
            'as'=> 'setting.index',
            'uses'=> 'Admin\SettingsController@index'
        ]);

        Route::get('/create',[
            'as'=> 'setting.create',
            'uses'=> 'Admin\SettingsController@create'
        ]);
        Route::post('/store',[
            'as'=> 'setting.store',
            'uses'=> 'Admin\SettingsController@store'
        ]);

        Route::get('/edit/{id}',[
            'as'=> 'setting_edit',
            'uses'=> 'Admin\SettingsController@edit'
        ]);
        Route::post('/update/{id}',[
            'as'=> 'setting_update',
            'uses'=> 'Admin\SettingsController@update'
        ]);

        Route::get('/delete/{id}',[
            'as'=> 'setting_detete',
            'uses'=> 'Admin\SettingsController@delete'
        ]);
    });

    Route::group(['prefix' => 'users'], function() {
        Route::get('/',[
            'as'=> 'user.index',
            'uses'=> 'Admin\UserController@index'
        ]);

        Route::get('/create',[
            'as'=> 'user.create',
            'uses'=> 'Admin\UserController@create'
        ]);
        Route::post('/store',[
            'as'=> 'user.store',
            'uses'=> 'Admin\UserController@store'
        ]);

        Route::get('/edit/{id}',[
            'as'=> 'user_edit',
            'uses'=> 'Admin\UserController@edit'
        ]);
        Route::post('/update/{id}',[
            'as'=> 'user_update',
            'uses'=> 'Admin\UserController@update'
        ]);

        Route::get('/delete/{id}',[
            'as'=> 'user_detete',
            'uses'=> 'Admin\UserController@delete'
        ]);
    });

    Route::group(['prefix' => 'roles'], function() {
        Route::get('/',[
            'as'=> 'role.index',
            'uses'=> 'Admin\RoleController@index'
        ]);

        Route::get('/create',[
            'as'=> 'role.create',
            'uses'=> 'Admin\RoleController@create'
        ]);
        Route::post('/store',[
            'as'=> 'role.store',
            'uses'=> 'Admin\RoleController@store'
        ]);

        Route::get('/edit/{id}',[
            'as'=> 'role_edit',
            'uses'=> 'Admin\RoleController@edit'
        ]);
        Route::post('/update/{id}',[
            'as'=> 'role_update',
            'uses'=> 'Admin\RoleController@update'
        ]);

        Route::get('/delete/{id}',[
            'as'=> 'role_detete',
            'uses'=> 'Admin\RoleController@delete'
        ]);
    });

});



