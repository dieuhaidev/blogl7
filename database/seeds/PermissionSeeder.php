<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['name' => 'Danh Mục Sản Phẩm', 'display_name'=>'Danh Mục Sản Phẩm', 'parent_id'=>0],
            ['name' => 'Danh Sách Danh Mục', 'display_name'=>'Danh sách danh mục', 'parent_id'=>1],
            ['name' => 'Thêm danh mục', 'display_name'=>'Thêm danh mục', 'parent_id'=>1],
            ['name' => 'Sửa danh mục', 'display_name'=>'Sửa danh mục', 'parent_id'=>1],
            ['name' => 'Xóa danh mục', 'display_name'=>'Xóa danh mục', 'parent_id'=>1],

            ['name' => 'Menu', 'display_name'=>'Menu', 'parent_id'=>0],
            ['name' => 'Danh sách menu', 'display_name'=>'Danh sách menu', 'parent_id'=>6],
            ['name' => 'Thêm menu', 'display_name'=>'Thêm menu', 'parent_id'=>6],
            ['name' => 'Sửa menu', 'display_name'=>'Sữa menu', 'parent_id'=>6],
            ['name' => 'Xóa menu', 'display_name'=>'Xóa menu', 'parent_id'=>6],

            ['name' => 'Slider', 'display_name'=>'Slider', 'parent_id'=>0],
            ['name' => 'Danh Sách Slider', 'display_name'=>'Danh Sách Slider', 'parent_id'=>11],
            ['name' => 'Thêm Slider', 'display_name'=>'Thêm Slider', 'parent_id'=>11],
            ['name' => 'Sữa Slider', 'display_name'=>'Sữa Slider', 'parent_id'=>11],
            ['name' => 'Xóa Slider', 'display_name'=>'Xóa Slider', 'parent_id'=>11],

            ['name' => 'Sản Phẩm', 'display_name'=>'Sản phẩm', 'parent_id'=>0],
            ['name' => ' Danh Sách Sản Phẩm', 'display_name'=>'Danh sách Sản phẩm', 'parent_id'=>16],
            ['name' => ' Thêm Sản Phẩm', 'display_name'=>'Thêm Sản phẩm', 'parent_id'=>16],
            ['name' => ' Sữa Sản Phẩm', 'display_name'=>'Sửa Sản phẩm', 'parent_id'=>16],
            ['name' => ' Xóa Sản Phẩm', 'display_name'=>'Xóa Sản phẩm', 'parent_id'=>16],

            ['name' => 'Setting', 'display_name'=>'setting', 'parent_id'=>0],
            ['name' => 'Danh Sách Setting', 'display_name'=>'Danh sách setting', 'parent_id'=>21],
            ['name' => 'Thêm Setting', 'display_name'=>'Thêm setting', 'parent_id'=>21],
            ['name' => 'Sửa Setting', 'display_name'=>'Sửa setting', 'parent_id'=>21],
            ['name' => 'Xóa Setting', 'display_name'=>'Xóa setting', 'parent_id'=>21],

            ['name' => 'Nhân viên', 'display_name'=>'Nhân viên', 'parent_id'=>0],
            ['name' => 'Danh sách nhân viên', 'display_name'=>'Danh sách nhân viên', 'parent_id'=>26],
            ['name' => 'Thêm nhân viên', 'display_name'=>'Thêm nhân viên', 'parent_id'=>26],
            ['name' => 'Sữa nhân viên', 'display_name'=>' Sữa nhân viên', 'parent_id'=>26],
            ['name' => 'Xóa nhân viên', 'display_name'=>' Xóa nhân viên', 'parent_id'=>26],

            ['name' => 'Vai trò', 'display_name'=>'Vai trò', 'parent_id'=>0],
            ['name' => 'Danh sách Vai trò', 'display_name'=>'Danh sách vai trò', 'parent_id'=>31],
            ['name' => 'Thêm Vai trò', 'display_name'=>'Thêm vai trò', 'parent_id'=>31],
            ['name' => 'Sửa Vai trò', 'display_name'=>'Sửa vai trò', 'parent_id'=>31],
            ['name' => 'Xóa Vai trò', 'display_name'=>'Xóa vai trò', 'parent_id'=>31],

        ]);
    }
}
