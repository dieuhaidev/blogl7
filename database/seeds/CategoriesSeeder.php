<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'Quần Áo', 'parent_id'=>'0', 'slug'=>'quan-ao'],
            ['name' => 'Giày Dép', 'parent_id'=>'0', 'slug'=>'giay-dep'],
            ['name' => 'Nón Mũ', 'parent_id'=>'0', 'slug'=>'non-mu'],
            ['name' => 'Thời Trang Nam', 'parent_id'=>'1', 'slug'=>'thoi-trang-nam'],
            ['name' => 'Thời Trang Nữ', 'parent_id'=>'1', 'slug'=>'thoi-trang-nu'],
        ]);
    }
}
