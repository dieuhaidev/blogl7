@extends('vendor.admin')
@section('title')
 <title>ADMIN | Dashboard</title>
@endsection
@section('css')
    <!-- FooTable -->
    <link href="{{ asset('css/plugins/footable/footable.core.css')}}" rel="stylesheet">
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{ asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{ asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
@endsection
@section('index')
<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
       <h2>E-commerce product list</h2>
       <ol class="breadcrumb">
           <li>
               <a href="index.html">Home</a>
           </li>
           <li>
               <a>E-commerce</a>
           </li>
           <li class="active">
               <strong>Product list</strong>
           </li>
       </ol>
   </div>
   <div class="col-lg-2">

   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Icon Buttons</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <p>
                    To buttons with any color or any size you can add extra icon on the left or the right side.
                </p>

                <h3 class="font-bold">Commom Icon Buttons</h3>
                <p>
                    <button class="btn btn-primary " type="button"><i class="fa fa-check"></i>&nbsp;Xuất Bản</button>
                    <button class="btn btn-warning " type="button"><i class="fa fa-times"></i>&nbsp;&nbsp;<span class="bold">Không Xuất Bản</span></button>
                    <a href="/admin/products-create"> <button class="btn btn-success " type="button"><i class="fa fa-plus"></i> Thêm</button></a>
                    <a href="/admin/products-edit"><button class="btn btn-warning " type="button"><i class="fa fa-edit"></i> <span class="bold">Sửa</span></button></a>
                    <button class="btn btn-danger " type="button"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Xóa</button>
            </div>
        </div> 
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1"> Thông Tin Sản Phẩm </a></li>
                <li class=""><a data-toggle="tab" href="#tab-2">Mô Tả Sản Phẩm</a></li>
                <li class=""><a data-toggle="tab" href="#tab-3">Ảnh Sản Phẩm</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <div class="ibox-content m-b-sm border-bottom">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="product_name">Tên Sản Phẩm</label>
                                        <input type="text" id="product_name" name="product_name" value="" placeholder="Product Name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="price">Giá Sản Phẩm</label>
                                        <input type="text" id="price" name="price" value="" placeholder="Price" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="quantity">Quantity</label>
                                        <input type="text" id="quantity" name="quantity" value="" placeholder="Quantity" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="quantity">Công Bố :</label>
                                        <input type="checkbox" checked  class="i-checks" id="" name="input[]" value="" placeholder="Quantity" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="quantity">Nổi Bật :</label>
                                        <input type="checkbox"  class="i-checks" id="" name="input[]" value="" placeholder="Quantity" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="status">Danh Mục</label>
                                        <select name="status" id="status" class="form-control" multiple>
                                            <option value="1" selected>Enabled</option>
                                            <option value="2">Disabled2</option>
                                            <option value="3">Disabled3</option>
                                            <option value="4">Disabled4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <div class="ibox-content m-b-sm border-bottom">
                            <div class="row">
                                <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Wyswig Summernote Editor</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="fa fa-wrench"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-user">
                                                <li><a href="#">Config option 1</a>
                                                </li>
                                                <li><a href="#">Config option 2</a>
                                                </li>
                                            </ul>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content no-padding">
                
                                        <div class="summernote">
                                            <h3>Lorem Ipsum is simply</h3>
                                            dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's</strong> standard dummy text ever since the 1500s,
                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                                            typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                                            <br/>
                                            <br/>
                                            <ul>
                                                <li>Remaining essentially unchanged</li>
                                                <li>Make a type specimen book</li>
                                                <li>Unknown printer</li>
                                            </ul>
                                        </div>
                
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>

                </div> 
                <div id="tab-3" class="tab-pane">
                    <div class="panel-body">
                        <div class="ibox-content m-b-sm border-bottom">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="product_name">Product Name</label>
                                        <input type="text" id="product_name" name="product_name" value="" placeholder="Product Name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="price">Price</label>
                                        <input type="text" id="price" name="price" value="" placeholder="Price" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="quantity">Quantity</label>
                                        <input type="text" id="quantity" name="quantity" value="" placeholder="Quantity" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="status">Status</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="1" selected>Enabled</option>
                                            <option value="0">Disabled</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
    
    
        </div>
    </div>
</div>

</div>
@endsection

@section('js')
<!-- FooTable -->
<script src="{{ asset('js/plugins/footable/footable.all.min.js')}}"></script>
<script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<!-- SUMMERNOTE -->
<script src="{{ asset('js/plugins/summernote/summernote.min.js')}}"></script>

<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
        });
        $('.summernote').summernote();

    });

</script>
@endsection