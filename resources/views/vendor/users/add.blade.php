@extends('vendor.admin')
@section('title')
<title>ADMIN | User</title>
@endsection
@section('css')
<link href="{{asset('css/plugins/dropzone/basic.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/dropzone/dropzone.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
@endsection
@section('index')
@include('vendor.layouts.content-header',['name'=>'Users','key'=>'Add'])


<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                          <i class="fa fa-wrench"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-user">
                          <li><a href="#">Config option 1</a>
                          </li>
                          <li><a href="#">Config option 2</a>
                          </li>
                      </ul>
                      <a class="close-link">
                          <i class="fa fa-times"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <form method="post" class="form-horizontal"  enctype='multipart/form-data' action="{{route('user.store')}}">
                    @csrf
                     <div class="form-group  @error('name') has-error @enderror "><label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" value="{{ old('name')}}" class="form-control">
                            @error('name') <label id="url-error" class="error"  >{{ $message }}</label> @enderror
                        </div>
                     </div>
                     <div class="form-group @error('email') has-error @enderror"><label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" name="email" value="{{ old('email')}}" class="form-control">
                            @error('email') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                        </div>
                     </div>
                     <div class="form-group @error('password') has-error @enderror"><label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="password" value="{{ old('password')}}" class="form-control">    
                            @error('password') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                        </div>
                     </div>
                     <div class="form-group"><label class="col-sm-2 control-label">Roles</label>
                       <div class="col-sm-5">
                            <select name="role_id[]" data-placeholder="Choose a Role..." class="chosen-select" multiple style="width:350px;" tabindex="4">
                                <option value="Select">Select</option>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                       </div>
                     </div>
                      <div class="hr-line-dashed"></div>
                      <div class="form-group">
                          <div class="col-sm-4 col-sm-offset-2">
                              <button class="btn btn-white" type="submit">Cancel</button>
                              <button class="btn btn-primary" type="submit">Save changes</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>
<script>
   Dropzone.options.dropzoneForm = {
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 2, // MB
      dictDefaultMessage: "<strong>Drop files here or click to upload."
   };
</script>
@endsection
@section('js')
<script src="{{asset('js/plugins/dropzone/dropzone.js')}}"></script>
<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>

<script>
    $(document).ready(function(){
        $('.chosen-select').chosen({width: "100%"});
    });
</script>
@endsection