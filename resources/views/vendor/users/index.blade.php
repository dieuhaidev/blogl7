@extends('vendor.admin')
@section('title')
 <title>ADMIN | Dashboard</title>
@endsection
@section('css')
    <!-- Sweet Alert -->
    <link href="{{asset('css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
@endsection
@section('index')
@include('vendor.layouts.content-header',['name'=>'Users','key'=>'List'])
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Custom responsive table </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4 m-b-xs"><select class="input-sm form-control input-s-sm inline">
                            <option value="0">Option 1</option>
                            <option value="1">Option 2</option>
                            <option value="2">Option 3</option>
                            <option value="3">Option 4</option>
                        </select>
                        </div>
                        <div class="col-sm-3 m-b-xs">
                            <div data-toggle="buttons" class="btn-group">
                                <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="options"> Day </label>
                                <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                        </div>
                        <div class="col-sm-2">
                            <a  class="btn btn-white btn-bitbucket" href="{{route('user.create')}}"><i class="fa fa-plus"> Add</i></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>

                                <th>#</th>
                                <th>#id</th>
                                <th>Danh Mục </th>
                                <th>Slug </th>
                                <th>Sữa</th>
                                <th>Xóa</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td><input type="checkbox"  checked class="i-checks" name="input[]"></td>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td><a href="{{route('user_edit',['id'=>$user->id])}}"><button type="button" class="btn btn-default btn-xs">Sửa</button></a></td>
                                        <td><a href="{{route('user_detete',['id'=>$user->id])}}"><button type="button" class="btn btn-danger btn-xs">Xóa</button></a></td> 
                                        <td><button 
                                            data-url="{{route('user_detete',['id'=>$user->id])}}" 
                                            data-fadethis ="fade{{$user->id}}"
                                            class="btn btn-danger btn-xs delete">Delete
                                        </button></td>
                                    </tr> 
                                @endforeach
                            
                            </tbody>
                        </table>
                        <div class='dataTables_paginate paging_simple_numbers'>{{ $users->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script>
    $(document).ready(function() {
              $('.delete').click(function () {
              let urlRequest =$(this).data('url');
              // let fadeThis   =$(this).data('fadethis');
              swal({
                      title: "Are you sure?",
                      text: "Your will not be able to recover this imaginary file!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Yes, delete it!",
                      cancelButtonText: "No, cancel plx!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                          $.ajax({
                                  type: "GET",
                                  url:urlRequest,
                                  success: function (data) {
                                      if(data.code ==200){
                                          // this.parent().parent().remove();
                                          swal("Deleted đã xóa!", "Your imaginary file has been deleted.", "success");
                                      }
                                  },
                                  error: function (){
  
                                  }
                          });
                          
                      } else {
                          swal("Cancelled", "Your imaginary file is safe :)", "error");
                      }
                  });
              });
      });
  </script>
@endsection