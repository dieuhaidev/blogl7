@extends('vendor.admin')
@section('title')
<title>ADMIN | Slider</title>
@endsection
@section('css')
<link href="{{asset('css/plugins/dropzone/basic.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/dropzone/dropzone.css')}}" rel="stylesheet">
@endsection
@section('index')
@include('vendor.layouts.content-header',['name'=>'Slider','key'=>'Edit'])


<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                          <i class="fa fa-wrench"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-user">
                          <li><a href="#">Config option 1</a>
                          </li>
                          <li><a href="#">Config option 2</a>
                          </li>
                      </ul>
                      <a class="close-link">
                          <i class="fa fa-times"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <form method="post" class="form-horizontal"  enctype='multipart/form-data' action="{{route('slider_update',['id'=>$slider->id])}}">
                    @csrf
                     <div class="form-group  @error('name') has-error @enderror "><label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" value="{{ $slider->name}}" class="form-control">
                            @error('name') <label id="url-error" class="error"  >{{ $message }}</label> @enderror
                        </div>
                     </div>
                     <div class="form-group @error('description') has-error @enderror"><label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" name="description" value="{{ $slider->description}}" class="form-control">
                            @error('description') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                        </div>
                     </div>
                     <div class="form-group @error('image_path') has-error @enderror"><label class="col-sm-2 control-label">Hình Ảnh</label>
                        <div class="col-sm-10">
                            <input type="file" name="image_path" value="{{  $slider->image_path }}" class="form-control">
                        <div  class="social-avatar"><img src="{{$slider->image_path}}" alt=""></div>
                            @error('image_path') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                        </div>
                     </div>
                      <div class="hr-line-dashed"></div>
                      <div class="form-group">
                          <div class="col-sm-4 col-sm-offset-2">
                              <button class="btn btn-white" type="submit">Cancel</button>
                              <button class="btn btn-primary" type="submit">Save changes</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>
<script>
   Dropzone.options.dropzoneForm = {
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 2, // MB
      dictDefaultMessage: "<strong>Drop files here or click to upload."
   };
</script>
@endsection
@section('js')
<script src="{{asset('js/plugins/dropzone/dropzone.js')}}"></script>
@endsection