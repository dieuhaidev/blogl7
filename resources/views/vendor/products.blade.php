@extends('vendor.admin')
@section('title')
 <title>ADMIN | Dashboard</title>
@endsection
@section('css')
    <!-- FooTable -->
    <link href="{{ asset('css/plugins/footable/footable.core.css')}}" rel="stylesheet">
    <link href="{{ asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@endsection
@section('index')
<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
       <h2>E-commerce product list</h2>
       <ol class="breadcrumb">
           <li>
               <a href="index.html">Home</a>
           </li>
           <li>
               <a>E-commerce</a>
           </li>
           <li class="active">
               <strong>Product list</strong>
           </li>
       </ol>
   </div>
   <div class="col-lg-2">

   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

    {{-- <div class="ibox-content m-b-sm border-bottom">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="product_name">Product Name</label>
                    <input type="text" id="product_name" name="product_name" value="" placeholder="Product Name" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="price">Price</label>
                    <input type="text" id="price" name="price" value="" placeholder="Price" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="quantity">Quantity</label>
                    <input type="text" id="quantity" name="quantity" value="" placeholder="Quantity" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="1" selected>Enabled</option>
                        <option value="0">Disabled</option>
                    </select>
                </div>
            </div>
        </div>
     
     </div> --}}

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Icon Buttons</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <p>
                    To buttons with any color or any size you can add extra icon on the left or the right side.
                </p>

                <h3 class="font-bold">Commom Icon Buttons</h3>
                <p>
                    <button class="btn btn-primary " type="button"><i class="fa fa-check"></i>&nbsp;Xuất Bản</button>
                    <button class="btn btn-warning " type="button"><i class="fa fa-times"></i>&nbsp;&nbsp;<span class="bold">Không Xuất Bản</span></button>
                    <a href="/admin/products-create"> <button class="btn btn-success " type="button"><i class="fa fa-plus"></i> Thêm</button></a>
                    <a href="/admin/products-edit"><button class="btn btn-warning " type="button"><i class="fa fa-edit"></i> <span class="bold">Sửa</span></button></a>
                    <button class="btn btn-danger " type="button"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Xóa</button>

                    {{-- <a class="btn btn-success btn-facebook">
                        <i class="fa fa-facebook"> </i> Sign in with Facebook
                    </a>
                    <a class="btn btn-success btn-facebook btn-outline">
                        <i class="fa fa-facebook"> </i> Sign in with Facebook
                    </a>
                    <a class="btn btn-white btn-bitbucket">
                        <i class="fa fa-user-md"></i>
                    </a>
                    <a class="btn btn-white btn-bitbucket">
                        <i class="fa fa-group"></i>
                    </a>
                    <a class="btn btn-white btn-bitbucket">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <a class="btn btn-white btn-bitbucket">
                        <i class="fa fa-exchange"></i>
                    </a>
                    <a class="btn btn-white btn-bitbucket">
                        <i class="fa fa-check-circle-o"></i>
                    </a>
                    <a class="btn btn-white btn-bitbucket">
                        <i class="fa fa-road"></i>
                    </a>
                    <a class="btn btn-white btn-bitbucket">
                        <i class="fa fa-ambulance"></i>
                    </a>
                    <a class="btn btn-white btn-bitbucket">
                        <i class="fa fa-star"></i> Stared
                    </a>
                </p>

                    <h3 class="font-bold">Toggle buttons Variations</h3>
                    <p>Button groups can act as a radio or a switch or even a single toggle. Below are some examples click to see what happens</p>
                <button data-toggle="button" class="btn btn-primary btn-outline" type="button">Single Toggle</button>
                <button data-toggle="button" class="btn btn-primary" type="button">Single Toggle</button>
                    <div data-toggle="buttons-checkbox" class="btn-group">
                        <button class="btn btn-primary active" type="button"><i class="fa fa-bold"></i> Bold</button>
                        <button class="btn btn-primary" type="button"><i class="fa fa-underline"></i> Underline</button>
                        <button class="btn btn-primary active" type="button"><i class="fa fa-italic"></i> Italic</button>
                    </div> --}}
            </div>
        </div> 
    </div>
</div>

<div class="row">
   <div class="col-lg-12">
       <div class="ibox">
           <div class="ibox-content">

               <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                   <thead>
                   <tr>

                       <th data-toggle="true">Product Name</th>
                       <th data-hide="phone">Model</th>
                       <th data-hide="all">Description</th>
                       <th data-hide="phone">Price</th>
                       <th data-hide="phone,tablet" >Quantity</th>
                       <th data-hide="phone">Status</th>
                       <th class="text-right" data-sort-ignore="true">Action</th>

                   </tr>
                   </thead>
                   <tbody>
                   <tr>
                       <td>
                          Example product 1
                       </td>
                       <td>
                           Model 1
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $50.00
                       </td>
                       <td>
                           1000
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 2
                       </td>
                       <td>
                           Model 2
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $40.00
                       </td>
                       <td>
                           4300
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 3
                       </td>
                       <td>
                           Model 3
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $22.00
                       </td>
                       <td>
                           300
                       </td>
                       <td>
                           <span class="label label-danger">Disabled</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 4
                       </td>
                       <td>
                           Model 4
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $67.00
                       </td>
                       <td>
                           2300
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 5
                       </td>
                       <td>
                           Model 5
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $76.00
                       </td>
                       <td>
                           800
                       </td>
                       <td>
                           <span class="label label-warning">Low stock</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 6
                       </td>
                       <td>
                           Model 6
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $60.00
                       </td>
                       <td>
                           6000
                       </td>
                       <td>
                           <span class="label label-danger">Disabled</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 7
                       </td>
                       <td>
                           Model 7
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $32.00
                       </td>
                       <td>
                           700
                       </td>
                       <td>
                           <span class="label label-danger">Disabled</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 8
                       </td>
                       <td>
                           Model 8
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $86.00
                       </td>
                       <td>
                           5180
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 9
                       </td>
                       <td>
                           Model 9
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $97.00
                       </td>
                       <td>
                           450
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 10
                       </td>
                       <td>
                           Model 10
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $43.00
                       </td>
                       <td>
                           7600
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 1
                       </td>
                       <td>
                           Model 1
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $50.00
                       </td>
                       <td>
                           1000
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 2
                       </td>
                       <td>
                           Model 2
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $40.00
                       </td>
                       <td>
                           4300
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 3
                       </td>
                       <td>
                           Model 3
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $22.00
                       </td>
                       <td>
                           300
                       </td>
                       <td>
                           <span class="label label-warning">Low stock</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 4
                       </td>
                       <td>
                           Model 4
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $67.00
                       </td>
                       <td>
                           2300
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 5
                       </td>
                       <td>
                           Model 5
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $76.00
                       </td>
                       <td>
                           800
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 6
                       </td>
                       <td>
                           Model 6
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $60.00
                       </td>
                       <td>
                           6000
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 7
                       </td>
                       <td>
                           Model 7
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $32.00
                       </td>
                       <td>
                           700
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 8
                       </td>
                       <td>
                           Model 8
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $86.00
                       </td>
                       <td>
                           5180
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 9
                       </td>
                       <td>
                           Model 9
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $97.00
                       </td>
                       <td>
                           450
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>
                   <tr>
                       <td>
                           Example product 10
                       </td>
                       <td>
                           Model 10
                       </td>
                       <td>
                           It is a long established fact that a reader will be distracted by the readable
                           content of a page when looking at its layout. The point of using Lorem Ipsum is
                           that it has a more-or-less normal distribution of letters, as opposed to using
                           'Content here, content here', making it look like readable English.
                       </td>
                       <td>
                           $43.00
                       </td>
                       <td>
                           7600
                       </td>
                       <td>
                           <span class="label label-primary">Enable</span>
                       </td>
                       <td class="text-right">
                           <div class="btn-group">
                               <button class="btn-white btn btn-xs">View</button>
                               <button class="btn-white btn btn-xs">Edit</button>
                           </div>
                       </td>
                       <td><input type="checkbox"  class="i-checks" name="input[]"></td>
                   </tr>


                   </tbody>
                   <tfoot>
                   <tr>
                       <td colspan="6">
                           <ul class="pagination pull-right"></ul>
                       </td>
                   </tr>
                   </tfoot>
               </table>

           </div>
       </div>
   </div>
</div>


</div>
@endsection

@section('js')
<!-- FooTable -->
<script src="{{ asset('js/plugins/footable/footable.all.min.js')}}"></script>
<script src="{{ asset('js/plugins/iCheck/icheck.min.js')}}"></script>

<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
        });

    });

</script>
@endsection