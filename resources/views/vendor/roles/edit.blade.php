@extends('vendor.admin')
@section('title')
<title>ADMIN | Role</title>
@endsection
@section('css')
<link href="{{asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">
@endsection
@section('index')
@include('vendor.layouts.content-header',['name'=>'Role','key'=>'Edit'])


<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                          <i class="fa fa-wrench"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-user">
                          <li><a href="#">Config option 1</a>
                          </li>
                          <li><a href="#">Config option 2</a>
                          </li>
                      </ul>
                      <a class="close-link">
                          <i class="fa fa-times"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <form method="post" class="form-horizontal"  enctype='multipart/form-data' action="{{route('role_update',['id'=>$role->id])}}">
                    @csrf
                     <div class="form-group  @error('name') has-error @enderror "><label class="col-sm-2 control-label">Name Role</label>
                        <div class="col-sm-4">
                            <input type="text" name="name" value="{{ $role->name }}" placeholder="Name Role" class="form-control">
                            @error('name') <label id="url-error" class="error"  >{{ $message }}</label> @enderror
                        </div>
                     </div>
                     <div class="form-group @error('display_name') has-error @enderror"><label class="col-sm-2 control-label">Mô Tả Role</label>
                        <div class="col-sm-10  @error('display_name') has-error @enderror">
                            <textarea name="display_name" class="form-control col-sm-2" placeholder="Mô Tả Role"  rows="3">{{$role->display_name}}</textarea>
                            @error('display_name') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                        </div>
                     </div>                                       
                        @foreach($permissionsParents as $permissionsParent)
                            <div class="form-group"><label class="col-sm-2 control-label">{{$permissionsParent->name}} : </label>
                                <div class="col-sm-2">
                                    <div class="checkbox checkbox-success checkbox-inline">
                                        <input type="checkbox" class="check checkAll">
                                        <label for="inlineCheckbox2"> Check All </label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    @foreach($permissionsParent->permissionsChildren as $permissionsChildItem)
                                        <div class="checkbox checkbox-success checkbox-inline">
                                            <input 
                                                type="checkbox" 
                                                name="permission_id[]" 
                                                class="check"
                                                value="{{$permissionsChildItem->id}}"
                                                {{$permissionsChecked->contains('id',$permissionsChildItem->id) ? 'checked' : ''}}>
                                            <label for="inlineCheckbox2"> {{$permissionsChildItem->name}} </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                          @endforeach
                       <div class="hr-line-dashed"></div>
                      <div class="form-group">
                          <div class="col-sm-4 col-sm-offset-2">
                              <button class="btn btn-white" type="submit">Cancel</button>
                              <button class="btn btn-primary" type="submit">Save changes</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>

@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('.checkAll').on('click', function () {
                $(this).parents('.form-group').find('.check').prop('checked', $(this).prop('checked'));
            });
        });
    </script>
@endsection
