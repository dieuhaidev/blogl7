@extends('vendor.admin')
@section('title')
 <title>ADMIN | Settings</title>
@endsection
@section('index')
@include('vendor.layouts.content-header',['name'=>'Settings','key'=>'Add'])


<div class="wrapper wrapper-content animated fadeInRight">
  
   <div class="row">
      <div class="col-lg-8">
         <div class="ibox float-e-margins">
             <div class="ibox-title">
                 <h5>Inline form</h5>
                 <div class="ibox-tools">
                     <a class="collapse-link">
                         <i class="fa fa-chevron-up"></i>
                     </a>
                     <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                         <i class="fa fa-wrench"></i>
                     </a>
                     <ul class="dropdown-menu dropdown-user">
                         <li><a href="#">Config option 1</a>
                         </li>
                         <li><a href="#">Config option 2</a>
                         </li>
                     </ul>
                     <a class="close-link">
                         <i class="fa fa-times"></i>
                     </a>
                 </div>
             </div>
             <div class="ibox-content">
                 <form role="form" action="{{route('setting.store').'?type='.request()->type}}" method="POST" class="form-inline">
                  @csrf
                     <div class="form-group">
                         <label for="exampleInputEmail2" class="sr-only">Config Key</label>
                         <input type="text" placeholder="Config key" id="exampleInputEmail2"
                                class="form-control"
                                name="config_key">
                                @error('config_key') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                     </div>
                     <div class="form-group">
                         <label for="exampleInputPassword2" class="sr-only">Config Value</label>
                         @if (request()->type === 'text')
                            <input type="text" 
                            placeholder="Config value"
                            class="form-control"
                            name="config_value">
                            @error('name') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                         @elseif (request()->type === 'textarea')
                            <textarea 
                                placeholder="Config value"
                                class="form-control"
                                name="config_value" cols="30" rows=""></textarea>
                                @error('config_value') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                         @endif
                     </div>
                     <div class="checkbox m-r-xs">
                         <input type="checkbox" id="checkbox1">
                         <label for="checkbox1">
                             Remember me
                         </label>
                     </div>
                     <button class="btn btn-white" type="submit">Sign in</button>
                 </form>
             </div>
         </div>
     </div>
   </div>
</div>
@endsection