@extends('vendor.admin')
@section('title')
 <title>ADMIN | Product Manager</title>
@endsection
@section('css')
<link href="{{asset('css/plugins/footable/footable.core.css')}}" rel="stylesheet">
<!-- Sweet Alert -->
<link href="{{asset('css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">

<link href="{{asset('css/animate.css')}}" rel="stylesheet">

@endsection

@section('index')
@include('vendor.layouts.content-header',['name'=>'Setttings','key'=>'List'])
<div class="wrapper wrapper-content animated fadeInRight ecommerce">


    <div class="ibox-content m-b-sm border-bottom">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="product_name">Product Name</label>
                    <input type="text" id="product_name" name="product_name" value="" placeholder="Product Name" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="price">Price</label>
                    <input type="text" id="price" name="price" value="" placeholder="Price" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="quantity">Quantity</label>
                    <input type="text" id="quantity" name="quantity" value="" placeholder="Quantity" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="1" selected>Enabled</option>
                        <option value="0">Disabled</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="status">Thêm sản phẩm</label>
                    <div>
                        <a class="btn btn-white btn-bitbucket" href="{{route('setting.create').'?type=text'}}"><i class="fa fa-plus"> Text</i></a>
                        <a class="btn btn-white btn-bitbucket" href="{{route('setting.create').'?type=textarea'}}"><i class="fa fa-plus"> Textarea</i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                        <thead>
                        <tr>
                            <th data-toggle="true">Config Value</th>
                            <th data-toggle="true">Config Name</th>
                            <th data-toggle="true">Config Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($settings  as $settingItem)
                        <tr class="fade{{$settingItem->id}}">
                            <td>
                                {{$settingItem->config_key}}
                            </td>
                        
                            <td>
                                {{$settingItem->config_value}}
                            </td>
                            <td>
                                {{$settingItem->type}}
                            </td>
                            <td>
                                <span class="label label-primary">Enable</span>
                                <span class="label label-danger">Disabled</span>
                                <span class="label label-warning">Low stock</span>
                            </td>
                           <td>
                            <button class="btn-white btn btn-xs"><a href="{{route('setting_edit',['id'=>$settingItem->id]).'?type='.$settingItem->type}}">Edit</a></button>
                            <button class="btn-white btn btn-xs"><a href="{{route('setting_detete',['id'=>$settingItem->id])}}">Delete</a></button>
                           </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('js')
    <script src="{{asset('js/inspinia.js')}}"></script>
    <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- Sweet alert -->
    <script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <!-- FooTable -->
    <script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>
    <script>
    
        $(document).ready(function() {
            $('.footable').footable();
            $('.delete').click(function () {
            let urlRequest =$(this).data('url');
            // let fadeThis   =$(this).data('fadethis');
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                                type: "GET",
                                url:urlRequest,
                                success: function (data) {
                                    if(data.code ==200){
                                        // this.parent().parent().remove();
                                        swal("Deleted đã xóa!", "Your imaginary file has been deleted.", "success");
                                    }
                                },
                                error: function (){

                                }
                        });
                       
                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                });
            });
        });
    </script>
@endsection