@extends('vendor.admin')
@section('title')
 <title>ADMIN | Product Manager</title>
@endsection
@section('css')
    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('index')
@include('vendor.layouts.content-header',['name'=>'Product','key'=>'Add'])
<div class="wrapper wrapper-content animated fadeInRight ecommerce">

    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Product info</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"> Data</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Discount</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4"> Images</a></li>
                    </ul>
                    <form action="{{route('products.store')}}" enctype='multipart/form-data' method="post">
                    @csrf
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">

                                    <fieldset class="form-horizontal">
                                        <div class="form-group @error('name') has-error @enderror"><label class="col-sm-2 control-label">Tên:</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="name" class="form-control" value="{{ old('name')}}"placeholder="Tên Sản Phẩm">
                                                @error('name') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                                            </div>
                                        </div>
                                        <div class="form-group @error('price') has-error @enderror"><label class="col-sm-2 control-label">Giá:</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="price" class="form-control" value="{{ old('price')}}" placeholder="160 000đ">
                                                @error('price') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                                            </div>
                                        </div>
                                        <div class="form-group @error('feature_image_path') has-error @enderror"><label class="col-sm-2 control-label">Hình ảnh đại diện:</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="feature_image_path" value="{{ old('feature_image_path')}}" class="form-control">
                                                 @error('feature_image_path') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                                            </div>
                                        </div>
                                        <div class="form-group @error('image_path') has-error @enderror"><label class="col-sm-2 control-label">Hình ảnh chi tiết</label>
                                            <div class="col-sm-10">
                                                <input type="file" multiple name="image_path[]" class="form-control" value="{{ old('image_path')}}" placeholder="$160.00">
                                                 @error('image_path') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                                            </div>
                                        </div>
                                        <div class="form-group @error('category_id') has-error @enderror"><label class="col-sm-2 control-label">Danh Mục</label>
                                            <div class="col-sm-10">
                                            <select class="form-control m-b" name="category_id">
                                                <option value="">Danh Mục</option>
                                               {{!!$categoryOptions!!}}
                                            </select>
                                            @error('category_id') <label id="url-error" class="error" >{{ $message }}</label> @enderror
                                            </div>
                                        </div> 

                                        <div class="form-group @error('feature_image_path') has-error @enderror"><label class="col-sm-2 control-label">Nội Dung:</label>
                                            <div class="col-sm-10">
                                            <textarea name="content" class="summernote">{{ old('content')}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="ibox">
                                                <div class="ibox-title">
                                                    <h5>Tags Input</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <p class="font-bold">
                                                        Thêm tags
                                                    </p>
                                                    <select name="tags[]" multiple class="tagsinput form-control" multiple="multiple"></select>
                                                    <!-- <input name="tags[]" multiple class="tagsinput form-control" data-role="tagsinput" type="text" /> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Meta Tag Title:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="..."></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Meta Tag Description:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="Sheets containing Lorem"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Meta Tag Keywords:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="Lorem, Ipsum, has, been"></div>
                                        </div>
                                    </fieldset>

                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">

                                    <fieldset class="form-horizontal">
                                        <div class="form-group"><label class="col-sm-2 control-label">ID:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="543"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Model:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="..."></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Location:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="location"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Tax Class:</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" >
                                                    <option>option 1</option>
                                                    <option>option 2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Quantity:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="Quantity"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Minimum quantity:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="2"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Sort order:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="0"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Status:</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" >
                                                    <option>option 1</option>
                                                    <option>option 2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>


                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <div class="panel-body">

                                    <div class="table-responsive">
                                        <table class="table table-stripped table-bordered">

                                            <thead>
                                            <tr>
                                                <th>
                                                    Group
                                                </th>
                                                <th>
                                                    Quantity
                                                </th>
                                                <th>
                                                    Discount
                                                </th>
                                                <th style="width: 20%">
                                                    Date start
                                                </th>
                                                <th style="width: 20%">
                                                    Date end
                                                </th>
                                                <th>
                                                    Actions
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <select class="form-control" >
                                                        <option selected>Group 1</option>
                                                        <option>Group 2</option>
                                                        <option>Group 3</option>
                                                        <option>Group 4</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="10">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="$10.00">
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                        <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control" >
                                                        <option selected>Group 1</option>
                                                        <option>Group 2</option>
                                                        <option>Group 3</option>
                                                        <option>Group 4</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="10">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="$10.00">
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control" >
                                                        <option selected>Group 1</option>
                                                        <option>Group 2</option>
                                                        <option>Group 3</option>
                                                        <option>Group 4</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="10">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="$10.00">
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control" >
                                                        <option selected>Group 1</option>
                                                        <option>Group 2</option>
                                                        <option>Group 3</option>
                                                        <option>Group 4</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="10">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="$10.00">
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control" >
                                                        <option selected>Group 1</option>
                                                        <option>Group 2</option>
                                                        <option>Group 3</option>
                                                        <option>Group 4</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="10">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="$10.00">
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control" >
                                                        <option selected>Group 1</option>
                                                        <option>Group 2</option>
                                                        <option>Group 3</option>
                                                        <option>Group 4</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="10">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="$10.00">
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control" >
                                                        <option selected>Group 1</option>
                                                        <option>Group 2</option>
                                                        <option>Group 3</option>
                                                        <option>Group 4</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="10">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" placeholder="$10.00">
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>

                                            </tbody>

                                        </table>
                                    </div>

                                </div>
                            </div>
                            <div id="tab-4" class="tab-pane">
                                <div class="panel-body">

                                    <div class="table-responsive">
                                        <table class="table table-bordered table-stripped">
                                            <thead>
                                            <tr>
                                                <th>
                                                    Image preview
                                                </th>
                                                <th>
                                                    Image url
                                                </th>
                                                <th>
                                                    Sort order
                                                </th>
                                                <th>
                                                    Actions
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <img src="img/gallery/2s.jpg">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="http://mydomain.com/images/image1.png">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="1">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="img/gallery/1s.jpg">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="http://mydomain.com/images/image2.png">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="2">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="img/gallery/3s.jpg">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="http://mydomain.com/images/image3.png">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="3">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="img/gallery/4s.jpg">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="http://mydomain.com/images/image4.png">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="4">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="img/gallery/5s.jpg">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="http://mydomain.com/images/image5.png">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="5">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="img/gallery/6s.jpg">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="http://mydomain.com/images/image6.png">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="6">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="img/gallery/7s.jpg">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="http://mydomain.com/images/image7.png">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="7">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </form>
            </div>
        </div>
    </div>

</div>
@endsection

@section('js')
    <!-- Custom and plugin javascript -->
<script src="{{asset('js/inspinia.js')}}"></script>
<script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>

<!-- SUMMERNOTE -->
<script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>

<!-- Data picker -->
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>

<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>

<script>
    $(document).ready(function(){

        $('.tagsinput').tagsinput({
            tagClass: 'label label-primary',
        });

        $('.summernote').summernote({height: 200});

        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    });
</script>
@endsection