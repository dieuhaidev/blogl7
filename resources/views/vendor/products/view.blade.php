@extends('vendor.admin')
@section('title')
 <title>ADMIN | Product Manager</title>
@endsection
@section('css')
<link href="{{asset('css/plugins/footable/footable.core.css')}}" rel="stylesheet">
@endsection

@section('index')
@include('vendor.layouts.content-header',['name'=>'Product','key'=>'View'])
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
views
</div>
@endsection

@section('js')
    <script src="{{asset('js/inspinia.js')}}"></script>
    <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- FooTable -->
    <script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>
    <script>
        $(document).ready(function() {

            $('.footable').footable();
        });



    </script>
@endsection