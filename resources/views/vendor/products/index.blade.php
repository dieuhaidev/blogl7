@extends('vendor.admin')
@section('title')
 <title>ADMIN | Product Manager</title>
@endsection
@section('css')
<link href="{{asset('css/plugins/footable/footable.core.css')}}" rel="stylesheet">
<!-- Sweet Alert -->
<link href="{{asset('css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">

<link href="{{asset('css/animate.css')}}" rel="stylesheet">

@endsection

@section('index')
@include('vendor.layouts.content-header',['name'=>'Product','key'=>'List'])
<div class="wrapper wrapper-content animated fadeInRight ecommerce">


    <div class="ibox-content m-b-sm border-bottom">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="product_name">Product Name</label>
                    <input type="text" id="product_name" name="product_name" value="" placeholder="Product Name" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="price">Price</label>
                    <input type="text" id="price" name="price" value="" placeholder="Price" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="quantity">Quantity</label>
                    <input type="text" id="quantity" name="quantity" value="" placeholder="Quantity" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="1" selected>Enabled</option>
                        <option value="0">Disabled</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="status">Thêm sản phẩm</label>
                    <div><a class="btn btn-white btn-bitbucket" href="{{route('products.create')}}"><i class="fa fa-plus"> Add</i></a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                        <thead>
                        <tr>
                            <th data-toggle="true">Tên Sản Phẩm</th>
                            <th data-toggle="true">Ảnh Sản Phẩm</th>
                            <th data-hide="phone">Danh Mục</th>
                            <th data-hide="all">Description</th>
                            <th data-hide="phone">Giá</th>
                            <th data-hide="phone,tablet" >Số Lượng</th>
                            <th data-hide="phone">Trạng Thái</th>
                            <th class="text-right" data-sort-ignore="true">Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products  as $productItem)
                        <tr class="fade{{$productItem->id}}">
                            <td>
                            {{$productItem->name}}
                            </td>
                            <td class="social-avatar">
                                <img alt="image" src="{{$productItem->feature_image_path}}">
                            </td>
                            <td>
                               {{optional($productItem->category)->name ? optional($productItem->category)->name : 'Không có danh mục'}} 
                            </td>
                            <td>
                                It is a long established fact that a reader will be distracted by the readable
                                content of a page when looking at its layout. The point of using Lorem Ipsum is
                                that it has a more-or-less normal distribution of letters, as opposed to using
                                'Content here, content here', making it look like readable English.
                            </td>
                            <td class='price'>
                            {{ number_format ($productItem->price)}} đ
                            </td>
                            <td>
                                1000
                            </td>
                            <td>
                                <span class="label label-primary">Enable</span>
                                <span class="label label-danger">Disabled</span>
                                <span class="label label-warning">Low stock</span>
                            </td>
                            <td class="text-right btn-group">
                                    <button class="btn-white btn btn-xs"><a href="{{route('product_view',['id'=>$productItem->id])}}">View</a></button>
                                    <button class="btn-white btn btn-xs"><a href="{{route('product_edit',['id'=>$productItem->id])}}">Edit</a></button>
                                    <button 
                                        data-url="{{route('product_delete',['id'=>$productItem->id])}}" 
                                        data-fadethis ="fade{{$productItem->id}}"
                                        class="btn btn-danger btn-xs delete">Delete</button>
                                    {{-- <!-- <a href="{{route('product_delete',['id'=>$productItem->id])}}">Delete</a> --> --}}
                            </td>
                            <td><input type="checkbox"  checked class="i-checks" name="input[]"></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td>
                            Example product 1
                            </td>
                            <td class="social-avatar">
                                <img alt="image" src="https://scontent.fsgn4-1.fna.fbcdn.net/v/t1.0-1/99297220_552022002163151_2424232975274082304_o.jpg?_nc_cat=103&_nc_sid=dbb9e7&_nc_ohc=3rZmFZU2x98AX_8S_nj&_nc_ht=scontent.fsgn4-1.fna&oh=36f5ffc3ad692122acc8d4912dec70a1&oe=5EF33CFE">
                            </td>
                            <td>
                                Model 1
                            </td>
                            <td>
                                It is a long established fact that a reader will be distracted by the readable
                                content of a page when looking at its layout. The point of using Lorem Ipsum is
                                that it has a more-or-less normal distribution of letters, as opposed to using
                                'Content here, content here', making it look like readable English.
                            </td>
                            <td>
                                $50.00
                            </td>
                            <td>
                                1000
                            </td>
                            <td>
                                <span class="label label-primary">Enable</span>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button class="btn-white btn btn-xs">View</button>
                                    <a href=""><button class="btn-white btn btn-xs">Edit</button></a>
                                    <button class="btn-white btn btn-xs">Delete</button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('js')
    <script src="{{asset('js/inspinia.js')}}"></script>
    <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- Sweet alert -->
    <script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <!-- FooTable -->
    <script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>
    <script>
    
        $(document).ready(function() {
            $('.footable').footable();
            $('.delete').click(function () {
            let urlRequest =$(this).data('url');
            // let fadeThis   =$(this).data('fadethis');
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                                type: "GET",
                                url:urlRequest,
                                success: function (data) {
                                    if(data.code ==200){
                                        // this.parent().parent().remove();
                                        swal("Deleted đã xóa!", "Your imaginary file has been deleted.", "success");
                                    }
                                },
                                error: function (){

                                }
                        });
                       
                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                });
            });
        });
    </script>
@endsection