@extends('vendor.admin')
@section('title')
 <title>ADMIN | Category Edit</title>
@endsection
@section('index')
@include('vendor.layouts.content-header',['name'=>'Category','key'=>'Edit'])


<div class="wrapper wrapper-content animated fadeInRight">
  
   <div class="row">
      <div class="col-lg-8">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5>Inline form</h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                          <i class="fa fa-wrench"></i>
                      </a>
                      {{--  <h1> </h1>  --}}
                      <ul class="dropdown-menu dropdown-user">
                          <li><a href="#">Config option 1</a>
                          </li>
                          <li><a href="#">Config option 2</a>
                          </li>
                      </ul>
                      <a class="close-link">
                          <i class="fa fa-times"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <form role="form" action="{{route('categories_update',['id'=>$category->id])}}" method="POST" class="form-inline">
                      @csrf
                    <div class="input-group m-b">
                        <input name="name" type="text" autocomplete="off" value ="{{$category->name}}" class="form-control">
                        <div class="input-group-btn">
                            <div class="form-group"><label class="col-sm-2 control-label">Select</label>
                                <select class="form-control m-b" name="parent_id">
                                    <option value="0">Danh Mục Cha</option>
                                    {!! $htmlOptions !!}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            {{-- <button class="btn btn-white" type="submit">Cancel</button> --}}
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                  </form>
              </div>
          </div>
      </div>
      <div class="col-lg-4">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5>Modal form <small>Example of login in modal box</small></h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                          <i class="fa fa-wrench"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-user">
                          <li><a href="#">Config option 1</a>
                          </li>
                          <li><a href="#">Config option 2</a>
                          </li>
                      </ul>
                      <a class="close-link">
                          <i class="fa fa-times"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <div class="text-center">
                  <a data-toggle="modal" class="btn btn-primary" href="#modal-form">Form in simple modal box</a>
                  </div>
                  <div id="modal-form" class="modal fade" aria-hidden="true">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-body">
                                  <div class="row">
                                      <div class="col-sm-6 b-r"><h3 class="m-t-none m-b">Sign in</h3>
   
                                          <p>Sign in today for more expirience.</p>
   
                                          <form role="form">
                                              <div class="form-group"><label>Email</label> <input type="email" placeholder="Enter email" class="form-control"></div>
                                              <div class="form-group"><label>Password</label> <input type="password" placeholder="Password" class="form-control"></div>
                                              <div>
                                                  <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Log in</strong></button>
                                                  <label> <input type="checkbox" class="i-checks"> Remember me </label>
                                              </div>
                                          </form>
                                      </div>
                                      <div class="col-sm-6"><h4>Not a member?</h4>
                                          <p>You can create an account:</p>
                                          <p class="text-center">
                                              <a href=""><i class="fa fa-sign-in big-icon"></i></a>
                                          </p>
                                  </div>
                              </div>
                          </div>
                          </div>
                      </div>
              </div>
          </div>
      </div>
   </div>
</div>
@endsection