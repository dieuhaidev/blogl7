@extends('vendor.admin')
@section('title')
 <title>ADMIN | Dashboard</title>
@endsection

@section('index')
@include('vendor.layouts.content-header',['name'=>'Menu','key'=>'List'])
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Custom responsive table </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4 m-b-xs"><select class="input-sm form-control input-s-sm inline">
                            <option value="0">Option 1</option>
                            <option value="1">Option 2</option>
                            <option value="2">Option 3</option>
                            <option value="3">Option 4</option>
                        </select>
                        </div>
                        <div class="col-sm-3 m-b-xs">
                            <div data-toggle="buttons" class="btn-group">
                                <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="options"> Day </label>
                                <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                        </div>
                        <div class="col-sm-2">
                           <a class="btn btn-white btn-bitbucket" href="{{route('menus.create')}}"><i class="fa fa-plus"> Add</i></a>
                       </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>

                                <th>#</th>
                                <th>#id</th>
                                <th>Danh Mục </th>
                                <th>Slug </th>
                                <th>Sữa</th>
                                <th>Xóa</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($menus as $menu)
                                        <tr>
                                            <td><input type="checkbox"  checked class="i-checks" name="input[]"></td>
                                            <td>{{$menu->id}}</td>
                                            <td>{{$menu->name}}</td>
                                            <td>{{$menu->slug}}</td>
                                            <td><a href="{{route('menu_edit',['id'=>$menu->id])}}"><button type="button" class="btn btn-default btn-xs">Sửa</button></a></td>
                                            <td><a href="{{route('menu_delete',['id'=>$menu->id])}}"><button type="button" class="btn btn-danger btn-xs">Xóa</button></a></td>
                                        </tr>
                                @endforeach
                                 {{-- <tr>
                                    <td><input type="checkbox"  checked class="i-checks" name="input[]"></td>
                                    <td>Project<small>This is example of project</small></td>
                                    <td><span class="pie">0.52/1.561</span></td>
                                    <td>20%</td>
                                    <td>Jul 14, 2013</td>
                                    <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                </tr>  --}}
                            
                            </tbody>
                        </table>
                        <div class='dataTables_paginate paging_simple_numbers'>{{ $menus->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection