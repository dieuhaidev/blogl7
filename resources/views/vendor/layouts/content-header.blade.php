<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{ $name .' '. $key }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">{{ $name }}</a>
            </li>
            <li class="active">
                <strong>{{ $key }}</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
 
    </div>
 </div>