<?php
namespace App\Http\Controllers\Admin;
use App\Role;
use App\Permission;
use App\Http\Requests\RoleAddRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct(Role $role, Permission $permission){
        $this->role        = $role;
        $this->permission =$permission;
    }

    public function index(){
        $roles = $this->role->paginate(10);
        return view('vendor.roles.index',compact('roles'));

    }
    public function create(){
        $permissionsParents = $this->permission->where('parent_id',0)->get();
        return view('vendor.roles.add',compact('permissionsParents'));
    }

    public function store(RoleAddRequest $request){
        $role = $this->role->create([
           'name'=>$request->name,
           'display_name'=>$request->display_name,
       ]);
       $role->permissions()->attach($request->permission_id);
       return redirect()->route('roles.index');
    }

    public function edit($id){
        $permissionsParents = $this->permission->where('parent_id',0)->get();
        $role = $this->role::find($id);
        $permissionsChecked = $role->permissions;
        return view('vendor.roles.edit',compact('permissionsParents','role','permissionsChecked'));
    }

    public function update(Request $request ,$id){
        $role = $this->role::find($id);
        $role->update([
            'name'=>$request->name,
            'display_name'=>$request->display_name,
        ]);
        
        $role->permissions()->sync($request->permission_id);
        return redirect()->route('roles.index');
    }

    public function delete(){
        \dd('ok');
    }
}
