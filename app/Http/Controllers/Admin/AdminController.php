<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{   
    public function adminLogin(){
        if(Auth::check()){
            return redirect()->to('admin/');
        }
        return view('vendor.login');
    }

    public function postAdminLogin(Request $request){
        // return view('vendor.login');
        $remember = $request->has('') ? true : false;
        if(Auth::attempt([
            'email'=>$request->email,
            'password'=>$request->password
        ],$remember)){

            return redirect()->to('admin/');

        }else{
            return redirect()->to('/');
        }
    }

    public function index(){
        return view('vendor.index');
       
    }


}
