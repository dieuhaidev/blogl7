<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\SliderAddRequest;

use App\Slider;
use App\Traits\StorageImageTraits;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SliderController extends Controller
{
    use StorageImageTraits;

    public function __construct(Slider $slider){
        $this->slider = $slider;
    }

    public function index(){
        $sliders =$this->slider->latest()->paginate(25);
        return view('vendor.sliders.index',compact('sliders'));
    }

    public function create(){
        return view('vendor.sliders.add');
    }
    public function edit($id){
        $slider =$this->slider::find($id);
        return view('vendor.sliders.edit',compact('slider'));
    }

    public function update(SliderAddRequest $request ,$id){
        try
        {
            $dataSliderUpdate =[
                'name'=>$request->name,
                'description'=>$request->description,
            ];
            $dataImageSlider = $this->StorageTraitUpload($request ,'image_path', 'slider');
            if(!empty($dataImageSlider)){
                $dataSliderUpdate['image_name'] = $dataImageSlider['file_name'];
                $dataSliderUpdate['image_path'] = $dataImageSlider['file_path'];
            }
            $this->slider->find($id)->update($dataSliderUpdate);
            return redirect()->route('slider.index');

        }
        catch (\Exception $exception) {
            Log::error('Lỗi: '. $exception->getMessage() . 'Line : ' . $exception->getLine());
        }
    }

    public function store(SliderAddRequest $request){
       
        try
        {
            $dataSliderInsert =[
                'name'=>$request->name,
                'description'=>$request->description,
            ];
            $dataImageSlider = $this->StorageTraitUpload($request ,'image_path', 'slider');
            if(!empty($dataImageSlider)){
                $dataSliderInsert['image_name'] = $dataImageSlider['file_name'];
                $dataSliderInsert['image_path'] = $dataImageSlider['file_path'];
            }
            $this->slider::create($dataSliderInsert);
            return redirect()->route('slider.index');

        }
        catch (\Exception $exception) {
            Log::error('Lỗi: '. $exception->getMessage() . 'Line : ' . $exception->getLine());
        }
    }

    public function delete($id){
        try{
            $this->slider::find($id)->delete();
            return response()->json([
                'code' =>200,
                'message' =>'sucess',  
            ],200);
        }catch (\Exception $exception) {
            Log::error('Message: '. $exception->getMessage() . 'Line : ' . $exception->getLine());
            return response()->json([
                'code' =>500,
                'message' =>'fail',  
            ],500);
        }
       
        // return view('vendor.products.index');
    }
}
