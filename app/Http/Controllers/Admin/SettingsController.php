<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\SettingAddRequest;

use App\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    private $setting;
    public function __construct(Setting $setting){
        $this->setting = $setting;
    }

    public function index(){
        $settings = $this->setting->latest()->paginate(25);
        return view('vendor.settings.index',compact('settings'));
    }

    public function create(){
        return view('vendor.settings.add');
    }

    public function store(SettingAddRequest $request){
        $this->setting::create([
            'config_key'=>$request->config_key,
            'config_value'=>$request->config_value,
            'type'=>$request->type,
        ]);
        return redirect()->route('setting.index');
    }

    public function edit($id){
        $setting =$this->setting::find($id);
        return view('vendor.settings.edit',compact('setting'));
    }

    public function update(SettingAddRequest $request , $id){
        try {
            $dataSettingUpdate =[
                'config_key'=>$request->config_key,
                'config_value'=>$request->config_value,
                'type'=>$request->type,
            ];
            $this->setting->find($id)->update($dataSettingUpdate);
            return redirect()->route('setting.index');

        } catch (\Exception $exception) {
            Log::error('Lỗi: '. $exception->getMessage() . 'Line : ' . $exception->getLine());
        }
    }

    public function delete($id){
        $this->setting::find($id)->delete();
        return redirect()->route('setting.index');
    }
}   
