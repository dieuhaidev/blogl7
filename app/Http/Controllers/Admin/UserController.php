<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserAddRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    private $user;
    private $role;

    public function __construct(User $user, Role $role){
        $this->user = $user;
        $this->role = $role;
    }

    public function index(){
        $users = $this->user->latest()->paginate(25);
        return view('vendor.users.index',compact('users'));
    }

    public function create(){
        $roles = $this->role::all();
        return view('vendor.users.add',compact('roles'));
    }

    public function store(UserAddRequest $request){
        try {
            DB::beginTransaction();
            $user = $this->user->create([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
            ]);
            $user->roles()->attach($request->role_id);
            DB::commit();
            return redirect()->route('user.index');
        } catch (\Exception $exceptionth) {
            DB::rollBack();
            Log::error('Messege :'. $exception->getMessage() . 'Line : ' . $exception->getLine());
        }
    }

    public function edit($id){
        $user = $this->user->find($id);
        $roles = $this->role::all();
        $rolesOfUser = $user->roles;
        return view('vendor.users.edit',compact('user','roles','rolesOfUser'));
    }

    public function update(Request $request , $id){
        try {
            DB::beginTransaction();
            $this->user->find($id)->update([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
            ]);
            $user = $this->user::find($id);
            $user->roles()->sync($request->role_id);
            DB::commit();
            return redirect()->route('user.index');
        } catch (\Exception $exceptionth) {
            DB::rollBack();
            Log::error('Messege :'. $exception->getMessage() . 'Line : ' . $exception->getLine());
        }
    }

    public function delete($id){
        try{
            $this->user::find($id)->delete();
            return response()->json([
                'code' =>200,
                'message' =>'sucess',  
            ],200);
        }catch (\Exception $exception) {
            Log::error('Message: '. $exception->getMessage() . 'Line : ' . $exception->getLine());
            return response()->json([
                'code' =>500,
                'message' =>'fail',  
            ],500);
        }
       
    }
}
