<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Components\MenuRecusive;
use App\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{   
    private $menuRecusive;
    private $menu;

    public function __construct(MenuRecusive $menuRecusive, Menu $menu){
        $this->menuRecusive  = $menuRecusive;
        $this->menu = $menu  = $menu;
    }

    public function index(){
        $menus =$this->menu->latest()->paginate(6);
        return view('vendor.menus.index',compact('menus'));
    }

    public function create(){ //Truyền giá trị rỗng if empty (tý em yêu)
        $htmlSelect = $this->menuRecusive->menuRecusiveAdd();
        return view('vendor.menus.add',compact('htmlSelect'));
    }

    public function store(Request $request){
        $this->menu->create([
            'name'=>$request->name,
            'parent_id'=>$request->parent_id,
            'slug'=>str_slug($request->input('name'))
        ]);
        return redirect()->route('menus.index');

    }

    public function edit($id){
        $menuFolowEdit = $this->menu->find($id);
        $optionSelect  =$this->menuRecusive->menuRecusiveEdit($menuFolowEdit->parent_id);
        return view('vendor.menus.edit',compact('menuFolowEdit', 'optionSelect'));

    }

    public function update(Request $request, $id){
       $this->menu::find($id)->update([
        'name'=>$request->name,
        'parent_id'=>$request->parent_id,
        'slug'=>str_slug($request->input('name'))
       ]);
        return redirect()->route('menus.index');     
    }

    public function delete($id){
        $this->menu::find($id)->delete();
        return redirect()->route('menus.index');
        
    }


}
