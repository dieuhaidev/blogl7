<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PruductAddRequest;

use App\Category;
use App\Product;
use App\ProductImage;
use App\Tag;
use App\ProductTag;
use App\Components\Recusive;
use App\Traits\StorageImageTraits;
use Illuminate\Support\Facades\Log;
use DB;


class ProductsController extends Controller
{
    use StorageImageTraits;

    private $category;
    private $product;
    private $productImage;
    private $tag;
    private $producttag;
    
    public function __construct(Category $category ,Product $product ,ProductImage $productImage, Tag $tag, ProductTag $producttag){
        $this->category     =$category;
        $this->product      =$product;
        $this->productImage =$productImage;
        $this->tag          =$tag;
        $this->producttag   =$producttag;
    }

    public function getCategory($parentId){
        $data =$this->category->all();
        $recursive   =new Recusive($data);
        $htmlOptions = $recursive->categoryRecusive($parentId);
        return $htmlOptions;
    }

    public function index(){
        $products = $this->product->latest()->paginate(25);
        return view('vendor.products.index',compact('products'));
    }

    public function view($id){
        return view('vendor.products.view');
    }

    public function create($parentId =''){
        $categoryOptions = $this->getCategory($parentId);
        return view('vendor.products.add',compact('categoryOptions'));
    }

    public function store(PruductAddRequest $request){

        try{
            DB::beginTransaction();
            $dataProductCreate =[
                'name'       =>$request->name,
                'price'      =>$request->price,
                'content'    =>$request->content,
                'user_id'    =>1,
                'category_id'=>$request->category_id,
            ];
            $dataUploadFeatureImage =$this->StorageTraitUpload($request,'feature_image_path', 'product' );
            if(!empty($dataUploadFeatureImage)){
                $dataProductCreate['feature_image_path'] = $dataUploadFeatureImage['file_path'];
                $dataProductCreate['feature_image_name'] = $dataUploadFeatureImage['file_name'];
            }
            $product = $this->product::create($dataProductCreate);
            // thêm hình ảnh vào product image
            if($request->hasFile('image_path')){
                foreach($request->image_path as $fileItem){
                    $dataProductImageDetails =$this->StorageTraitUploadMutiple($fileItem, 'product');
                    $product->images()->create([ //https://laravel.com/docs/7.x/eloquent-relationships#the-create-method
                        'image_path'=>$dataProductImageDetails['file_path'],
                        'image_name'=>$dataProductImageDetails['file_name'],
                    ]);
                }
            }
            // Thêm tag cho sản phẩm
            if(!empty($request->tags)){
                foreach ($request->tags as $tagItem) {
                    $tagstances = $this->tag::firstOrCreate(['name'=>$tagItem]); //https://laravel.com/docs/7.x/eloquent#other-creation-methods
                    $tagIds[] = $tagstances->id;
                }
            }
           
            $product->tags()->attach($tagIds); //https://laravel.com/docs/7.x/eloquent-relationships#updating-many-to-many-relationships
            DB::commit();
            return redirect()->route('products.index');

        }catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Message: '. $exception->getMessage() . 'Line : ' . $exception->getLine());
        }

    }

    public function edit($id){
        $product =$this->product::find($id);
        $htmlOptions = $this->getCategory($product->category_id);
        return view('vendor.products.edit',compact('htmlOptions','product'));
    }

    public function update(Request $request , $id){
        try{
            DB::beginTransaction();
            $dataProductUpdate =[
                'name'       =>$request->name,
                'price'      =>$request->price,
                'content'    =>$request->content,
                'user_id'    =>1,
                'category_id'=>$request->category_id,
            ];
            $dataUploadFeatureImage =$this->StorageTraitUpload($request,'feature_image_path', 'product' );
            if(!empty($dataUploadFeatureImage)){
                $dataProductUpdate['feature_image_path'] = $dataUploadFeatureImage['file_path'];
                $dataProductUpdate['feature_image_name'] = $dataUploadFeatureImage['file_name'];
            }
            $this->product->find($id)->update($dataProductUpdate);
            $product = $this->product::find($id);
            // thêm hình ảnh vào product image
            if($request->hasFile('image_path')){
                $this->productImage->where('product_id',$id)->delete(); // Xóa image cũ
                foreach($request->image_path as $fileItem){
                    $dataProductImageDetails =$this->StorageTraitUploadMutiple($fileItem, 'product');
                    $product->images()->create([ //https://laravel.com/docs/7.x/eloquent-relationships#the-create-method
                        'image_path'=>$dataProductImageDetails['file_path'],
                        'image_name'=>$dataProductImageDetails['file_name'],
                    ]);
                }
            }
            // Thêm tag cho sản phẩm
            if(!empty($request->tags)){
                foreach ($request->tags as $tagItem) {
                    $tagstances = $this->tag::firstOrCreate(['name'=>$tagItem]); //https://laravel.com/docs/7.x/eloquent#other-creation-methods
                    $tagIds[] = $tagstances->id;
                }
            }
           
            $product->tags()->sync($tagIds); //https://laravel.com/docs/7.x/eloquent-relationships#updating-many-to-many-relationships
            DB::commit();
            return redirect()->route('products.index');

        }catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Message: '. $exception->getMessage() . 'Line : ' . $exception->getLine());
        }

        return view('vendor.products.index');
    }
    public function delete($id){
        try{
            $this->product::find($id)->delete();
            return response()->json([
                'code' =>200,
                'message' =>'sucess',  
            ],200);
        }catch (\Exception $exception) {
            Log::error('Message: '. $exception->getMessage() . 'Line : ' . $exception->getLine());
            return response()->json([
                'code' =>500,
                'message' =>'fail',  
            ],500);
        }
       
        // return view('vendor.products.index');
    }
}
