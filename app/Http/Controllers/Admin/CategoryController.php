<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Http\Request;
use App\Components\Recusive;

class CategoryController extends Controller{  
    
    private $category;
    
    public function __construct(Category $category){
        $this->category =$category;
    }

    public function getCategory($parentId){
        $data =$this->category->all();
        $recursive   =new Recusive($data);
        $htmlOptions = $recursive->categoryRecusive($parentId);
        return $htmlOptions;
    }

    public function index(){
        $categories  =$this->category->latest()->paginate(10);
        return view('vendor.category.index',compact('categories'));
    }

    public function create($parentId = ''){ //Truyền giá trị rỗng if empty (tý em yêu)
        $htmlOptions = $this->getCategory($parentId);
        return view('vendor.category.add',compact('htmlOptions'));
    }

    public function store(Request $request){
        $this->category->create([
            'name'=>$request->input('name'),
            'parent_id'=>$request->get('parent_id'),
            'slug'=>str_slug($request->input('name')),
        ]);
        return redirect()->route('categories.index');
    }

    public function edit(Request $request, $id){
        $category =$this->category::find($id);
        $htmlOptions = $this->getCategory($category->parent_id);
        return view('vendor.category.edit',compact('category','htmlOptions'));
    }

    public function update($id, Request $request){
        $this->category::find($id)->update([
            'name'=>$request->input('name'),
            'parent_id'=>$request->get('parent_id'),
            'slug'=>str_slug($request->input('name')),
        ]);
        return redirect()->route('categories.index');
    }

    public function delete($id){
        $this->category::find($id)->delete();
        return redirect()->route('categories.index');
    }

}
