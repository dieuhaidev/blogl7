<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'bail|required|max:255|min:5',
            'description'=>'bail|required|max:255|min:10',
            'image_path'=>'bail|required',
            
        ];
    }

    public function messages(){

        return [
            'name.required' => 'Không được phép để trống',
            'name.max' => 'Không được phép qua 255 kí tự',
            'name.min' => 'Không được phép ít hơn 5 kí tự',

            'description.required' => 'Không được phép để trống',
            'description.max' => 'Không được phép qua 11 kí tự',
            'description.min' => 'Không được phép ít hơn 4 kí tự',

            'image_path.required' => 'Không được phép để trống',
        ];

    }
}
