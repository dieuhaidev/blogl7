<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PruductAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'bail|required|max:255|min:10',
            'price'=>'required|max:11|min:4',
            'category_id'=>'required',
            'feature_image_path'=>'required',
            'image_path'=>'required',
            'content'=>'required',
        ];
    }

    public function messages()
        {
            return [
                'name.required' => 'Không được phép để trống',
                'name.max' => 'Không được phép qua 255 kí tự',
                'name.min' => 'Không được phép ít hơn 10 kí tự',
    
                'price.required' => 'Không được phép để trống',
                'price.max' => 'Không được phép qua 11 kí tự',
                'price.min' => 'Không được phép ít hơn 4 kí tự',
    
                'category_id.required' => 'Không được phép để trống',
    
                'feature_image_path.required' => 'Không được phép để trống',
    
                'image_path.required' => 'Không được phép để trống',
    
                'content.required' => 'Không được phép để trống',
            ];
        }
}
