<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'bail|required|max:255|min:5',
            'email'=>'bail|required|max:255|min:10|unique:users',
            'password'=>'bail|required',
        ];
    }

    public function messages(){

        return [
            'name.required' => 'Không được phép để trống',
            'name.max' => 'Không được phép qua 255 kí tự',
            'name.min' => 'Không được phép ít hơn 5 kí tự',

            'email.required' => 'Không được phép để trống',
            'email.max' => 'Không được phép qua 11 kí tự',
            'email.min' => 'Không được phép ít hơn 4 kí tự',
            'email.unique' => 'email đã được đăng ký',

            'password.required' => 'Không được phép để trống',
        ];

    }
}
