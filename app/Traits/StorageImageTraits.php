<?php
namespace App\Traits;
use Storage;

trait StorageImageTraits {
   public function StorageTraitUpload($request, $fieldName, $foderName){

      if($request->hasFile($fieldName)){
         $file = $request->$fieldName;
         $fileNameOrigin =$file->getClientOriginalName();
         $fileNamHash = str_random(20). '.' . $file->getClientOriginalExtension();
         $fileNamPath = $request->file($fieldName)->storeAs('public/' .$foderName .'/'. auth()->id(), $fileNamHash);
         $dataUploadTrait =[
            'file_name'=>$fileNameOrigin,
            'file_path'=>Storage::url($fileNamPath)
         ];
         return $dataUploadTrait;
      }
      return null;

   }

   public function StorageTraitUploadMutiple($file, $foderName){

      $fileNameOrigin =$file->getClientOriginalName();
      $fileNamHash = str_random(20). '.' . $file->getClientOriginalExtension();
      $fileNamPath = $file->storeAs('public/' .$foderName .'/'. auth()->id(), $fileNamHash);
      $dataUploadTrait =[
         'file_name'=>$fileNameOrigin,
         'file_path'=>Storage::url($fileNamPath)
      ];
      return $dataUploadTrait;

   }
}