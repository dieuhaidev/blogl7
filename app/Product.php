<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;//xóa mềm

class Product extends Model
{
    use SoftDeletes;
    protected $fillable=['name','price','feature_image_path','content','user_id','feature_image_name','category_id'];

    public function images(){
        return $this->hasMany(ProductImage::class, 'product_id');
    }

    public function tags(){
        return $this
            ->belongsToMany(Tag::class, 'product_tags','product_id','tag_id')
            ->withtimestamps();
    }

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }

    public function productImage(){
        return $this->hasMany(ProductImage::class,'product_id');
    }

}
